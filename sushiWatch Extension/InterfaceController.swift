//
//  InterfaceController.swift
//  sushiWatch Extension
//
//  Created by Mahammad on 2019-11-05.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController, WCSessionDelegate {
    
    @IBOutlet weak var buttonGroup: WKInterfaceGroup!
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
      func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
    
    let name = message["name"] as! String
        print("\(name)")
    }

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        if (WCSession.isSupported() == true) {
                  // create a communication session with the phone
                  let session = WCSession.default
                  session.delegate = self
                  session.activate()
        }
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
  
    
    @IBAction func moveLeft() {
        print("left button pressed")
        if(WCSession.default.isReachable == true){
                   let message = ["name":"left"] as [String : Any]
                   WCSession.default.sendMessage(message, replyHandler: nil)
                   }
                   else {
                   }
        
        
    }
    
    @IBAction func moveRight() {
        print("right button pressed")
        
        if(WCSession.default.isReachable == true){
        let message = ["name":"right"] as [String : Any]
        WCSession.default.sendMessage(message, replyHandler: nil)
        }
        else {
        }
        
    }
    
}
